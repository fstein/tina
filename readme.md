# Dokumentation des TINA - Prototyps

## Systemzusammenfassung
Die Hardware des Prototyps besteht diesen Komponenten:
* Hauptplatine
   Die Funktionen der Hauptplatine sind:
   * Stromversorgung des Systems (16.0V, 10.4V, 3.8V, 3.3V, 5V, -7V)
   * Batterieladung
   * Interfacing zwischen dem Raspberry Pi und den Bildschirmen.

* Zwei Bildschirme
   * Die Bildschirme sind baugleich und spiegelsymmetrisch verbaut. Sie sind über eine gemeinsame Schnittstelle am Raspberry Pi angeschlossen und zeigen somit immer dasselbe Bild.
   * Die Hintergrundbeleuchtung der Bildschirme kann vom Raspberry Pi per PWM gedimmt werden.

* Batterie
    * Die Batterie hat eine Kapazität von 5200mAh bei 7.4V nominaler Spannung
    * Die Batterie kann mit einem Schiebeschalter auf der Unterseite des Geräts elektrisch vom System getrennt werden. Sie wird dann auch nicht geladen, wenn ein Adapter angeschlossen ist. Der Schiebeschalter dient auch zum booten den Raspberry Pi.

* Reset-Taste
    - Auf der Unterseite des Geräts befindet sich eine kleine Reset-Taste. Wird diese länger als 5s aber unter 15s gedrückt, rebootet das System. Wird sie länger als 15s gedrückt, fährt das System herunter und kann dann mit dem Schiebeschalter vom Strom getrennt werden.

* Raspberry Pi zero W
    * Raspbian Stretch ohne Desktopumgebung ist installiert und für die Nutzung in diesem Gerät konfiguriert. 
    * Eine Nutzung ausserhalb der TINA ist nicht vorgesehen. Vor allem der HDMI-Ausgang ist in dieser Konfiguration nicht mehr nutzbar. 
    * Die Konfiguration findet hauptsächlich über /boot/config.txt und einen angepassten device-tree-blob statt. Ausserdem sind die Libraries für Qt 5.10 installiert.

* RTC Modul
    * am Raspberry pi ist ein Uhr-Modul mit eigener Batterie angeschlossen, das für die Richtige Systemzeit sorgt, wenn kein Internet verfügbar ist.

* TINA-Anwendung
    - Nach dem Booten des Raspberry pi wird die TINA-Anwendung gestartet. Erst hier wird das Hintergrundlicht der Displays eingeschaltet. Die BLE-Schnittstelle ist dann sichtbar und man kann sich mit der Anwendung verbinden.

## BLE-Schnittstelle
Zur Steuerung der TINA-Anwendung kommuniziert man über die nachfolgend beschriebene Schnittstelle.
Der Prototyp hat die physische Adresse `B8:27:EB:C0:C0:6A`

Der TINA-Service hat die UUID `beabd118-0f3e-11e9-ab14-d663bd873d93`
Im Service sind drei Charakteristiken enthalten:

1. `c7f377e5-0f3e-11e9-ab14-d663bd873d93`
    Eigenschaften: read
    Kurzname: E5
    Aus dieser Charakteristik können Eigenschaften ausgelesen werden.

2. `c7f377e6-0f3e-11e9-ab14-d663bd873d93`
    Eigenschaften: write
    Kurzname: E6
    In diese Charakteristik können Befehle geschrieben werden.

3. `c7f377e7-0f3e-11e9-ab14-d663bd873d93`
    Eigenschaften: read, notify
    Kurzname: E7
    Aus dieser Charakteristik können Informationen über das Meeting ausgelesen werden. Man kann diese Informationen abonnieren. Diese Charakteristik aktualisiert sich bei einem laufenden Meeting ein Mal pro Sekunde.

Um Befehle an die TINA-Anwendung zu schicken, schreibt man ein Paket mit maximal 20 Byte in die Charakteristik `E6`.

Befehlsstruktur:
```
index       Bezeichnung
[0]         TYPE CMD
[1]         ID
[2]         APPEND
[3]-[19]    PAYLOAD
```

Über den Wert von `TYPE CMD` gibt man den Befehlstyp an:
```
Wert    Bezeichnung
1       Item mit Dauer Hinzufügen
2       Owner String setzen/erweitern
3       Goal String setzen/erweitern
4       Titel String setzen/erweitern
5       Cost setzen
6       Steuerung
7       letztes Item entfernen
```

Um ein neues Item anzulegen, erstellt man es zunächst mit dem Wert 1 auf Index `[0]` und der Dauer in Sekunden innerhalb der Payload:
```
index   Inhalt
[3]     uint32 highest byte (big endian)
[4]     uint32
[5]     uint32
[6]     uint32 lowest byte (big endian)
```

Die `ID` wird nach dem Erfolgreichen Anlegen eines neuen Items auf der Charakteristik `E5` in dieser Form bereitgestellt:
```
index   Wert
[0]     1     
[1]     ID
```
Diese muss für die weitere Bearbeitung des Items gesichert werden.

Um die Items mit Inhalt zu füllen, nutzt man die Befehle `TYPE CMD = 2,3 und 4`. Auf Index `[1]` gibt man mit der zuvor erhaltenen `ID` an, welches Item bearbeitet werden soll. Mit dem Byte `APPEND` steuert man, ob der String angehängt werden soll, oder ob es sich um die erste Übertragung handelt.
Ist `APPEND == 1` wird der text in diesem Paket an den bereits vorhandenen String angehangen. So kann man Strings übertragen, die länger sind als die verfügbaren 16 Bytes in einem Paket. Das erste Byte der Payload muss die Länge des Strings im selben Paket beinhalten:
```
index       Wert
[3]         String Länge     
[4]-[19]    String
```


Um die Kosten pro Sekunde für das Meeting zu setzen, nutzt man den Befehl `TYPE CMD == 5`. Hier muss keine `ID` angegeben werden. Die Kosten werden als float mit 4 Byte Größe übertragen:
```
index   Wert
[3]     float highest byte (big endian)
[4]     float
[5]     float
[6]     float lowest byte (big endian)
```

Um das Meeting zu starten und zu steuern, nutzt man den Befehl `TYPE CMD == 6`. Über das Byte mit Index `[3]` wird dann die Aktion definiert:
```
Wert    Aktion
0x01    Meeting beenden    
0x02    vorheriges Item
0x04    nächstes Item
0x08    Meeting pausieren
0x10    Meeting starten/fortfahren
0x20    System Herunterfahren
```

Hat man auf der Charakteristik `E7` notifications abonniert, haben diese die folgende Struktur:
```
index   Struktur
[0]     uint32 highest byte (big endian) 
[1]     uint32 
[2]     uint32 
[3]     uint32 lowest byte (big endian) 
[4]     uint32 highest byte (big endian) 
[5]     uint32 
[6]     uint32 
[7]     uint32 lowest byte (big endian)
[8]     uint8 aktive ID
```

Die übertragenen Werte repräsentieren das Meeting:
```
index       Inhalt
[0]-[3]     Meetingdauer in Sekunden
[4]-[7]     Meetingkosten
[8]         ID des aktiven Item
```

Zur Referenz ist in diesem Repository unter `/res/ble-demo.py` eine Beispielhafte Steuerung in Python implementiert.

Das Protokoll ist zusätzlich in `190117_protokollstruktur.ods` tabellarisch aufgeführt.

## System Setup
Der aktuelle login ist:
User:pi
Passwort:rebel

Um eine neue installation aufzusetzen:u
Ausgangspunkt is ein Raspberry Pi zero W mit Raspbian stretch. Das Raspberry Pi sollte mit autologin in die Kommandozeile booten und das Dateisystem expandiert sein.
Der GPIO Header muss so angelötet sein, dass die Pins auf der **Unterseite** herausschauen.
Anschließend sind die folgenden Schritte vorzusehen:

+ `/res/config.txt` nach `/boot/config.txt` kopieren
+ eine der beiden `*.bin` Dateien in die `/boot/` partition auf dem pi kopieren und umbenennen zu `dt-blob.bin`:
    * `/res/dt-blob_with_display_off.bin`
    * `/res/dt-blob_with_display_on.bin`
+ `/res/shutdown.py` nach `/home/pi/shutdown.py` auf dem pi kopieren
+ `sudo python3 /home/pi/shutdown.py &` zu `/etc/rc.local` vor `exit 0` hinzufügen
+ Qt-Anwendung `qt-test-2` in `/home/pi/qt-test-2/bin/qt-test-2` kopieren
+ `./home/pi/qt-test-2/bin/qt-test-2&` zu `/etc/rc.local` vor `exit 0` hinzufügen
+ Die Entwicklungsumgebung mit Crosscompile wurde nach dieser Anleitung aufgesetzt: https://lb.raspberrypi.org/forums/viewtopic.php?f=75&t=204778
+ Wenn Qt Entwicklung nicht benötigt wird, kann `qt5pi.tar.bz2` nach `/usr/local/qt5pi` auf dem pi entpackt werden.
+ Das Skript `displaysToggleRST.sh` setzt die Displaycontroller zurück. Das wird automatisch gemacht, wenn eine der beiden `dt-blob.bin` installiert ist.
+ Das Skript `enabledisplay.sh` schaltet die Hintergrundbeleuchtung der Displays ein.

## Gerätehandhabung
Der Prototyp ist mit Vorsicht zu behandeln. Wenn nicht erforderlich, ist er nicht zu öffnen. Sollte es notwendig sein, muss der Deckel vorsichtig angehoben werden, damit die daran befestigten Kabel nicht beschädigt werden. 

#### Einschalten
Den Schiebeschalter auf der Unterseite von `OFF` auf `ON` schieben. Nach ca.20s startet die TINA-Anwendung und das TINA-MEETINGS Logo erscheint auf dem Display.

#### Ausschalten
1. Das System über die BLE-Schnittstelle herunterfahren oder den Taster auf der Unterseite länger als 15s drücken
2. ca. 10s warten
3. Den Schiebenschalter auf der Unterseite von `ON` auf `OFF` schieben

#### Laden
1. Schiebeschalter auf `ON` stellen, damit die Batterie elektrisch mit dem System verbunden ist. 
2. Ladegerät in die Netzsteckdose einstecken
3. Ladegerät am Gerät anschließen
4. Die Ladezeit beträgt ca. 3 Stunden. In dieser Zeit das Gerät nicht unbeaufsichtig lassen. 
5. Nach dem Laden das Netzteil abstecken
6. Schiebeschalter auf `OFF` stellen

## Sonstiges
* Die Displaygläser sind nur mit dem beiliegenden Mikrofasertuch zu reinigen. Bei starken Verschmutzungen kann das Tuch mit Wasser angefeuchtet werden. **Keine Reiniger mit Alkohol verwenden!**
