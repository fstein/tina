# This script will wait for a button to be pressed and then shutdown or reboot the Raspberry Pi. A long press initiates a reboot, a very long press initiates a shutdown.

import time
from time import sleep
import RPi.GPIO as GPIO
import os

##############CONFIG##########################
demo_mode = False
debug = True
GPIN = 10
##############END CONFIG######################

GPIO.setmode(GPIO.BCM)

# Pin 21 will be input and will have its pull-up resistor (to 3V3) activated
# so we only need to connect a push button with a current limiting resistor to ground
GPIO.setup(GPIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
int_active=0

print("shutdown/reboot script started.")


# ISR: if our button is pressed, we will have a falling edge on pin 21
# this will trigger the interrupt:
def shutdown_reboot_callback(channel):
    # button is pressed
    # possibly shutdown our Raspberry Pi
    global int_active
    # only react when there is no other shutdown process running

    if (int_active == 0):
        int_active = 1

    pressed = 1
    shutdown = 0

    # count how long the button is pressed
    counter = 0

    while (pressed == 1):
        if (GPIO.input(GPIN) == False):
            # button is still pressed
            counter = counter + 1
            if debug:
                print("pressed: " + str(counter))
                # break if we count beyond 20 (long-press is a shutdown)
                if (counter >= 15):
                    pressed = 0
                else:
                    sleep(0.2)
        else:
            # button has been released
            pressed = 0

    # button has been released, count cycles and determine action

    # count how long the button was pressed
    if (counter < 2):
        # short press, do nothing
        pressed = 0
        int_active = 0
        if debug:
            print(str("short press, do nothing"))
    elif counter <= 5:
        if debug:
            print("else " + str(counter))
        # longer press
        # medium length press, initiate system reboot
        if debug:
            print("rebooting..")

        # run the reboot command as a background process
        if demo_mode:
            print("sudo reboot now")
            GPIO.cleanup()
            exit(0)
        else:
            os.system("sudo reboot")

    elif counter > 5 and counter <= 15:
    # long press, initiate system shutdown
        if debug:
            print("shutting down..")

        # run the shutdown command as a background process
        if demo_mode:
            print("sudo shutdown now")
            GPIO.cleanup()
            exit(0)
        else:
            os.system("sudo shutdown now")


# Program pin 21 as an interrupt input:
# it will react on a falling edge and call our interrupt routine "shutdown_reboot_callback"
GPIO.add_event_detect(GPIN, GPIO.FALLING, callback=shutdown_reboot_callback, bouncetime=500)
print("-")
while True:
    #if debug:
        #print("alive")
    time.sleep(1)
