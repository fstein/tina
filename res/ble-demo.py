import curses
from bluepy import btle
import struct
import time

class MyDelegate(btle.DefaultDelegate):
        def __init__(self, tracker):
            btle.DefaultDelegate.__init__(self)
            self.trck = tracker

        def handleNotification(self, cHandle, data):
            self.trck.receiveNotification(data)

class tina:
    def __init__(self, uuid):
        self._notify = 0
        self.uuid = uuid
        self._delegate = MyDelegate(self)
        self._peripheral = 0
        self._service = 0
        self._E5 = 0
        self._E6 = 0
        self._E7 = 0
        self._running = False

    @property
    def running(self):
        return self._running

    def connect(self):
        self._peripheral = btle.Peripheral(self.uuid, btle.ADDR_TYPE_PUBLIC, 0)
        self._peripheral.setDelegate(self._delegate)

        self._service = self._peripheral.getServiceByUUID("beabd118-0f3e-11e9-ab14-d663bd873d93")
        self._E5 = self._service.getCharacteristics("c7f377e5-0f3e-11e9-ab14-d663bd873d93")[0]
        self._E6 = self._service.getCharacteristics("c7f377e6-0f3e-11e9-ab14-d663bd873d93")[0]
        self._E7 = self._service.getCharacteristics("c7f377e7-0f3e-11e9-ab14-d663bd873d93")[0]

    def enable_notify(self):
        if self._peripheral:
            self._peripheral.writeCharacteristic(self._E7.getHandle() + 1, struct.pack('<bb', 0x01, 0x00))
            self._notify = 1
        else:
            pass

    def disable_notify(self):
        if self._peripheral:
            self._peripheral.writeCharacteristic(self._E7.getHandle() + 1, struct.pack('<bb', 0x00, 0x00))
            self._notify = 0
        else:
            pass

    def send_cmd(self, _cmd):
        if self._peripheral:
            self._E6.write(_cmd, withResponse=True)
        else:
            pass

    def nextTp(self):
        self.send_cmd(struct.pack(">BBBB", 0x06, 0x00, 0x00, 0x04))

    def shutdown(self):
        self.send_cmd(struct.pack(">BBBB", 0x06, 0x00, 0x00, 0x20))


    def prevTp(self):
        self.send_cmd(struct.pack(">BBBB", 0x06, 0x00, 0x00, 0x02))


    def setGlobalCost(self, cost):
        self.send_cmd(struct.pack(">BBBf", 0x05, 0x00, 0x00, cost))


    def addItem(self, duration):
        self.send_cmd(struct.pack(">BBBL", 0x01, 0x00, 0x00, duration))
        #time.sleep(1)
        resp = self._E5.read()
        resp_0 = struct.unpack("<BBxxxxxxxxxxxxxxxxxx", resp)
        if resp_0 [0] == 1:
            return resp_0[1]
        else:
            return -1

    def setString(self, type, id, sendString, append=0):
        #type = 2 owner
        #type = 3 goal
        #type = 4 description

        if len(sendString)>17:
            #truncate
            length = len(sendString)
            packets = [sendString[i:i+17] for i in range(0, length, 17)]
            for i, packet in enumerate(packets):
                #print("sent: {}:{}:{}".format(i,packet.encode(),1 if i>0 else 0))
                self.send_cmd(struct.pack(">BB?B{}s".format(len(packet)), type, id, 1 if i>0 else 0,len(packet), packet.encode()))
        else:
            self.send_cmd(struct.pack(">BB?B{}s".format(len(sendString)), type, id, append,len(sendString), sendString.encode()))
            #print("sent String: {}".format(sendString))

    def setOwnerWithId(self, id, owner):
        self.setString(0x02, id, owner)

    def setGoalWithId(self, id, goal):
        self.setString(0x03, id, goal)

    def setDescriptionWithId(self, id, desc):
        self.setString(0x04, id, desc)

    def removeItemLastItem(self):
        self.send_cmd(struct.pack(">B", 0x07))

    def playMeeting(self):
        self.send_cmd(struct.pack(">BxxB", 0x06, 0x10))
        self._running = True
        #print("play")

    def pauseMeeting(self):
        self.send_cmd(struct.pack(">BxxB", 0x06, 0x08))
        self._running = False
        #print("pause")

    def toggleRunning(self):
        if self._running:
            self.pauseMeeting()
        else:
            self.playMeeting()
            self.enable_notify()

    def endMeeting(self):
        self.send_cmd(struct.pack(">BxxB", 0x06, 0x01))
        #print("end")


    def waitForNotifications(self, duration):
        return self._peripheral.waitForNotifications(duration)

    def attachNotifyCallback(self, callback):
        self._callback = callback

    def receiveNotification(self, data):
        if self._callback:
            values = struct.unpack(">IIB", data)
            self._callback(values)
        else:
            pass
        #print("received Notification with length: {}".format(len(data)))
        #print("data: {}".format(struct.unpack(">II", data)))

def bleDemo(stdscr):
    stdscr.clear()
    stdscr.nodelay(1)
    curses.curs_set(0)

    mac = "B8:27:EB:C0:C0:6A"

    begin_x = 5
    begin_y = 5
    width = 45
    height = 30
    win = curses.newwin(height, width, begin_y, begin_x)


    def putText():
        win.clear()
        win.addstr(1,3, "{:^38}".format("TINA control"))
        win.hline(2,1,curses.ACS_HLINE,width)
        win.addstr(4,3,"p\tplay/pause")
        win.addstr(5,3,"w\tprevious item")
        win.addstr(6,3,"s\tnext item")
        win.addstr(7,3,"a\tadd item")
        win.addstr(8,3,"e\tset meeting cost")
        win.addstr(9,3,"x\tstop meeting")
        win.addstr(10,3,"d\tenable/disable notifications")
        win.addstr(11,3,"r\tadd default talking point")
        win.addstr(12,3,"q\tshut down tina")
        win.hline(13,1,curses.ACS_HLINE,width)

        win.hline(height-7,1,curses.ACS_HLINE,width)
        win.hline(height-3,1,curses.ACS_HLINE,width)
        draw()

    def draw():
        win.border()
        win.refresh()

    putText()

    def myGetRawInput(line=0, prompt_string="Enter value:"):
        curses.echo()
        win.addstr(line, 3, prompt_string)
        input = win.getstr(line + 1, 3, 40)
        curses.noecho()
        return input.decode()

    def log(msg=""):
        win.move(height-2,3)
        win.clrtoeol()
        win.addstr(msg)
        #draw()

    def writeNotify(data):
        win.move(height-4,3)
        win.clrtoeol()
        win.addstr("duration: {}s".format(data[0]))
        win.move(height-5,3)
        win.clrtoeol()
        win.addstr("cost: {}€".format(data[1]))
        win.move(height-6,3)
        win.clrtoeol()
        win.addstr("item: {}".format(data[2]))
        draw()



    log("connecting to device {}".format(mac))
    t = tina(mac)
    t.connect()
    log("connected")
    t.attachNotifyCallback(writeNotify)

    while True:
        if t.waitForNotifications(0.3):
            continue
        c = stdscr.getch()
        if c == ord("p"):
            t.toggleRunning()
        if c == ord("w"):
            t.prevTp()

        if c == ord("r"):
            # add default TP
            id = t.addItem(15)
            if id >0:
                log("new item id: {}".format(id))
                t.setDescriptionWithId(id, "default title {}".format(id))
                t.setOwnerWithId(id, "default owner")
                t.setGoalWithId(id, "default goal")
                t.setGlobalCost(12.3)
                log("default item added")

        if c == ord("a"):
            stdscr.nodelay(0)
            curses.curs_set(1)
            title = myGetRawInput(15, prompt_string="title/description:")
            owner = myGetRawInput(17, prompt_string="owner:")
            goal = myGetRawInput(19, prompt_string="goal:")
            duration = myGetRawInput(21, prompt_string="duration in seconds:")

            log("adding item")
            log(duration)
            id = t.addItem(int(duration.strip()))
            if id >0:
                log("new item id: {}".format(id))
                t.setDescriptionWithId(id, title)
                t.setOwnerWithId(id, owner)
                t.setGoalWithId(id, goal)
                log("item added")
            putText()
            stdscr.nodelay(1)
            curses.curs_set(0)
            pass

        if c == ord("e"):
            stdscr.nodelay(0)
            curses.curs_set(1)
            cost = myGetRawInput(19, prompt_string="meeting cost per second: ")
            t.setGlobalCost(float(cost.replace(",",".")))
            log("cost set")
            stdscr.nodelay(1)
            curses.curs_set(0)

        if c == ord("s"):
            t.nextTp()
        if c == ord("d"):
            t.enable_notify()
        if c == ord("x"):
            t.endMeeting()

        if c == ord("q"):
            t.shutdown()
        draw()

def main(stdscr):
    bleDemo(stdscr)


if __name__ == "__main__":
    curses.wrapper(main)
